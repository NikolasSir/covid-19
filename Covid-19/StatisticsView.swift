//
//  StatisticsView.swift
//  Covid-19
//
//  Created by N Sh on 25.05.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI

struct StatisticsView: View {
    private(set) var description: String
    private(set) var data: MainData
    private(set) var daily: [Daily]
    private(set) var last: Int

    var body: some View {
        VStack{
            VStack{
                Text(self.description)
                    .foregroundColor(.black)
                    .fontWeight(.bold)
                    .font(.largeTitle)
                HStack(spacing: 16){
                    VStack(spacing: 12){
                        Text("Affected")
                            .fontWeight(.bold)
                        
                        Text("\(self.data.cases)")
                            .fontWeight(.bold)
                            .font(.title)
                    }
                    .padding(.vertical)
                    .frame(width: (UIScreen.main.bounds.width/2) - 30)
                    .background(Color(#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)))
                    .cornerRadius(20)
                    
                    VStack(spacing: 12){
                        Text("Deaths")
                            .fontWeight(.bold)
                        
                        Text("\(self.data.deaths)")
                            .fontWeight(.bold)
                            .font(.title)
                    }
                    .padding(.vertical)
                    .frame(width: (UIScreen.main.bounds.width/2) - 30)
                    .background(Color(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)))
                    .cornerRadius(20)
                }
                .foregroundColor(.white)
                .padding(.top, 10)
                
                HStack(spacing: 16){
                    VStack(spacing: 12){
                        Text("Recovered")
                            .fontWeight(.bold)
                        
                        Text("\(self.data.recovered)")
                            .fontWeight(.bold)
                    }
                    .padding(.vertical)
                    .frame(width: (UIScreen.main.bounds.width/3) - 30, height: UIScreen.main.bounds.height/10)
                    .background(Color(#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)))
                    .cornerRadius(20)
                    
                    VStack(spacing: 12){
                        Text("Active")
                            .fontWeight(.bold)
                        
                        Text("\(self.data.active)")
                            .fontWeight(.bold)
                    }
                    .padding(.vertical)
                    .frame(width: (UIScreen.main.bounds.width/3) - 30, height: UIScreen.main.bounds.height/10)
                    .background(Color(#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)))
                    .cornerRadius(20)
                    
                    VStack(spacing: 12){
                        Text("Serious")
                            .fontWeight(.bold)
                        
                        Text("\(self.data.critical)")
                            .fontWeight(.bold)
                    }
                    .padding(.vertical)
                    .frame(width: (UIScreen.main.bounds.width/3) - 30, height: UIScreen.main.bounds.height/10)
                    .background(Color(#colorLiteral(red: 0.4955972433, green: 0.5346659422, blue: 0.9638351798, alpha: 1)))
                    .cornerRadius(20)
                }
                .foregroundColor(.white)
                .padding(.vertical, 10)
            }
                
                VStack(spacing: 15){
                    
                    HStack{
                        Text("Last 7 days")
                            .font(.title)
                            .foregroundColor(.black)
                    }
                    
                    HStack{
                        if self.daily.isEmpty || self.daily.last?.cases == 0{
                            EmptyView()
                        } else {
                        ForEach(self.daily){ day in
                            VStack(spacing: 10){
                                Text(day.cases > 100000 ? "\(day.cases / 1000)K" : "\(day.cases)")
                                    .lineLimit(1)
                                    .font(.caption)
                                    .foregroundColor(.gray)
                                GeometryReader{ g in
                                    VStack{
                                        Spacer(minLength: 0)
                                        Rectangle()
                                            .fill(Color(#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)))
                                            .frame(width: 15, height: self.getHeight(last: self.last, value: day.cases, height: g.frame(in: .global).height))
                                    }
                                }
                                Text("\(day.day)")
                                    .lineLimit(1)
                                    .font(.caption)
                                    .foregroundColor(.gray)
                            }
                        }
                    }
                    }
                }
            }
           // .background(Color(#colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)))
           // .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
          //  .cornerRadius(10)
            .padding(.bottom, 10)
            //.shadow(color: .black, radius: 5, x: 5, y: 5)
          //  .shadow(color: .black, radius: 5, x: -5, y: -5)
    }
    
    func getHeight(last: Int, value: Int, height: CGFloat) -> CGFloat{
         if last != 0{
             let converted = CGFloat(value) / CGFloat(last)
             return converted * height
         } else {
             return 0
         }
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        StatisticsView(description: "", data: MainData.init(deaths: 0, recovered: 0, active: 0, critical: 0, cases: 0), daily: [], last: 0)
    }
}
