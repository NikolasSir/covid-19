//
//  CovidGlobalModalView.swift
//  Covid-19
//
//  Created by N Sh on 26.05.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI

struct CovidGlobalModalView: View {
    @Environment(\.presentationMode) var presentationMode
    private(set) var data: MainData
    private(set) var daily: [Daily]
    private(set) var last: Int
    
    var body: some View {
        VStack{
            StatisticsView(description: "Global", data: data, daily: daily, last: last)
                .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height*0.85)
                .background(Color(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
                .shadow(color: .white, radius: 5, x: 5, y: 5)
                .shadow(color: .white, radius: 5, x: -5, y: -5)
            Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }){
                Text("Back")
                    .foregroundColor(.white)
                    .padding(.vertical, 12)
                    .frame(width: (UIScreen.main.bounds.width/2) - 30)
            }
            .background(Color(#colorLiteral(red: 0.9467939734, green: 0.9468161464, blue: 0.9468042254, alpha: 1)))
            .clipShape(Capsule())
        }
        .edgesIgnoringSafeArea(.all)
       // .background(Color(#colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)))
        
    }
    
}

struct CovidGlobalModalView_Previews: PreviewProvider {
    static var previews: some View {
        CovidGlobalModalView(data: MainData.init(deaths: 0, recovered: 0, active: 0, critical: 0, cases: 0), daily: [], last: 0)
    }
} 
