//
//  ContentView.swift
//  Covid-19
//
//  Created by N Sh on 20.05.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        CovidView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
