//
//  JSONModels.swift
//  Covid-19
//
//  Created by N Sh on 24.05.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation

//Data models for JSON

struct Daily: Identifiable {
    var id: Int
    var day: String
    var cases: Int
}

struct MainData: Decodable{
    var deaths: Int
    var recovered: Int
    var active: Int
    var critical: Int
    var cases: Int
}

struct MyCounty: Decodable{
    var timeline: [String : [String: Int]]
}

struct Global: Decodable{
    var cases: [String: Int]
}

