//
//  NetworkService.swift
//  Covid-19
//
//  Created by N Sh on 24.05.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation

class NetworkService{
    
    var dailyCount: Int = 0
    var countCountry: Int = 0
    var dictData: [String: MainData] = [:]
    var dictDaily: [String: [Daily]] = [:]
    
    func getMainDataFromCountry(country: String, completion: @escaping (MainData) -> ()){
        var countryModified: String = ""
        let isEmptySpaceHere: Bool = country.contains(" ")
        if isEmptySpaceHere{
            countryModified = country.replacingOccurrences(of: " ", with: "%20")
        }
        guard let url = URL(string: "https://corona.lmao.ninja/v2/countries/\(isEmptySpaceHere ? countryModified : country)?yesterday=false") else { return }
        
        let session = URLSession(configuration: .default)
        session.dataTask(with: url){
            (data, _, error) in
            var mainData: MainData?
            if error != nil {
                print((error?.localizedDescription)!)
                return
            }
            
            do{
                let json = try JSONDecoder().decode(MainData.self, from: data!)
                mainData = json
            } catch {
                print(country)
                print(countryModified)
            }
            
            DispatchQueue.main.async {
                completion(mainData!)
            }
            self.countCountry += 1
            self.dictData[country] = mainData
//            print(country)
//            print(mainData!)
            print(self.countCountry)
//            print(self.dictData)
            print(self.dictData.count)
        }
        .resume()
    }
    
    func getDailyDataFromCountry(country: String, completion: @escaping ([Daily], Int) -> ()){
        var countryModified: String = ""
        let isEmptySpaceHere: Bool = country.contains(" ")
        if isEmptySpaceHere{
            countryModified = country.replacingOccurrences(of: " ", with: "%20")
        }
        guard let url = URL(string: "https://corona.lmao.ninja/v2/historical/\(isEmptySpaceHere ? countryModified : country)?lastdays=7") else { return }
        var daily: [Daily] = []
        var count = 0
        var cases: [String: Int] = [:]
        
        
        let session = URLSession(configuration: .default)
        session.dataTask(with: url){
            (data,_,error) in
            if error != nil {
                print((error?.localizedDescription)!)
                return
            }
    
            do {
                let json = try JSONDecoder().decode(MyCounty.self, from: data!)
                cases = json.timeline["cases"]!
            } catch{
                daily.append(Daily(id: 0, day: "", cases: 0))
            }
            
            for i in cases{
                daily.append(Daily(id: count, day: i.key, cases: i.value))
                count += 1
            }
            
            daily.sort{(first, second) -> Bool in
                if first.day < second.day{
                    return true
                } else {
                    return false
                }
            }
            
            DispatchQueue.main.async {
                completion(daily, daily.last!.cases)
            }
            self.dictDaily[country] = daily
            self.dailyCount += 1
//            print(country)
//            print(daily)
            print(self.dailyCount)
            print(self.dictDaily.count)
        }
        .resume()
    }
    
    func getMainDataFromGlobal(completion: @escaping (MainData) -> ()){
        guard let url = URL(string: "https://corona.lmao.ninja/v2/all?today") else { return }
        let session = URLSession(configuration: .default)
        session.dataTask(with: url){
            (data, _, error) in
            if error != nil {
                print((error?.localizedDescription)!)
                return
            }
            
            let json = try! JSONDecoder().decode(MainData.self, from: data!)
            
            DispatchQueue.main.async {
                completion(json)
            }
        }
        .resume()
    }

    func getDailyDataFromGlobal(completion: @escaping ([Daily], Int) -> ()){
      guard let url = URL(string: "https://corona.lmao.ninja/v2/historical/all?lastdays=7") else { return }
      var daily: [Daily] = []
      let session = URLSession(configuration: .default)
      session.dataTask(with: url){(data,_,error) in
          if error != nil {
              print((error?.localizedDescription)!)
              return
          }
          
          var count = 0
          var cases: [String: Int] = [:]
          
          let json = try! JSONDecoder().decode(Global.self, from: data!)
          cases = json.cases
          
          for i in cases{
              daily.append(Daily(id: count, day: i.key, cases: i.value))
              count += 1
          }
          
          daily.sort{(first, second) -> Bool in
              if first.day < second.day{
                  return true
              } else {
                  return false
              }
          }
          
          DispatchQueue.main.async {
            completion(daily, daily.last!.cases)
          }
      }
      .resume()
  } 
}
