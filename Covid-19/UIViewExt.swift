//
//  UIViewExt.swift
//  Covid-19
//
//  Created by N Sh on 28.05.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation
import SwiftUI

extension UIView{
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
    }
}
