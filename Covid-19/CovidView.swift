//
//  CovidView.swift
//  Covid-19
//
//  Created by N Sh on 23.05.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import SwiftUI

struct CovidView: View {
    private let count = 201
    @State var globalData: MainData = MainData.init(deaths: 0,
                                                    recovered: 0,
                                                    active: 0,
                                                    critical: 0,
                                                    cases: 0)
    @State var globalDaily: [Daily] = []
    @State var isPresented: Bool = false
    @State var data: [String: MainData] = [:]
    @State var daily: [String: [Daily]] = [:]
    @State var lasts: [String: Int] = [:]
    @State private var search: String = ""
    private(set) var service: NetworkService = NetworkService()
    
    var body: some View {
        VStack{
            if self.data.count == count && self.daily.count == count {
                SearchBar(text: self.$search)
                NavigationView{
                    List(countryList.filter{ self.search.isEmpty ? true : $0.contains(search) }, id: \.self){ country in
                        NavigationLink(destination: StatisticsView(description: country, data: self.data[country] ?? MainData.init(deaths: 0, recovered: 0, active: 0, critical: 0, cases: 0), daily: self.daily[country] ?? [], last: self.lasts[country] ?? 0)){
                            Text(country)
                        }
                    }
                    .navigationBarTitle(Text("Statistics COVID-19")
                        .font(.largeTitle),
                                         displayMode: .inline)
                        .navigationBarItems(trailing:
                            Button(action: {
                                self.service.getMainDataFromGlobal(){ data in
                                    self.globalData = data
                                }
                                self.service.getDailyDataFromGlobal(){ daily, _ in
                                    self.globalDaily = daily
                                }
                                //                                                        self.data.removeAll()
                                //                                                        self.daily.removeAll()
                                //                                                        self.lasts.removeAll()
                                self.isPresented.toggle()
                            }){
                                Text("Global")
                                    .padding(.vertical, 12)
                            }
                            .sheet(isPresented: self.$isPresented){
                                CovidGlobalModalView(data: self.globalData,
                                                     daily: self.globalDaily,
                                                     last: self.globalDaily.last?.cases ?? 0)
                                    .onAppear{
                                        self.service.getMainDataFromGlobal(){ data in
                                            self.globalData = data
                                        }
                                        self.service.getDailyDataFromGlobal(){ daily, _ in
                                            self.globalDaily = daily
                                        }
                                }
                                
                        })
                }
            }
            else {
                Indicator()
            }
        }.onAppear{
                for country in countryList{
                    self.service.getMainDataFromCountry(country: country){ countryData in
                        self.data[country] = countryData
                    }
                    self.service.getDailyDataFromCountry(country: country){ countryDaily, last in
                        self.daily[country] = countryDaily
                        self.lasts[country] = last
                    }
                }
            }
    }
}

struct CovidCountryView_Previews: PreviewProvider {
    static var previews: some View {
        CovidView()
    }
}
